package com.numerical

import scala.math._

class IntegrationMethodComparator {

  def error(exact:Double, approximate:Double) : Double = {
    return abs(exact-approximate)/exact * 100
  }
}
