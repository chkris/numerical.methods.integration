package com.numerical

import com.numerical._

object Container {

  def main(args : Array[String]) {
    val a = -1.8
    val b = 2.0
    val n = 3
    var integrationMethodList = Array(new IntegrationAnalitycal, new RectangularIntegration , new TrapezoidIntegration , new NewtonCotesIntegration, new ChebyshevIntegration)
    val methodComparator = new IntegrationMethodComparator

    for(method <- integrationMethodList) {
      print(s"${method.getName()} = ${method.approximate(a, b, n)} error= ${methodComparator.error(integrationMethodList(0).approximate(a, b, n), method.approximate(a, b, n))}\n")
    }
  }
}
