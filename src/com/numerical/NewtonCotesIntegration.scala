package com.numerical

import scala.math._

class NewtonCotesIntegration extends Integration {

  override def getName() : String = {"newton-cotes"}

  override def approximate(a:Double, b:Double, n:Int) : Double = {
    var result:Double = 0.0

    val h:Double = (abs(b) + abs(a))/n
    val C =  new Array[Double](n+1)
    // value of parameter "C" calculated analytically
    if(n==1) {
      C(0) = 0.5
      C(1) = 0.5
    }

    // value of parameter "C" calculated analytically
    if(n==3) {
      C(0) = 1.0/8.0
      C(3) = 1.0/8.0
      C(1) = 3.0/8.0
      C(2) = 3.0/8.0
    }
    for(i <- 0 to n)
    {
      result = result + C(i) * f(a+i*h); // calculate sum needed to calculate
    }

    return (b-a)*result // calculate integral using Newton-Cotes
  }
}
