package com.numerical

import scala.math._

class ChebyshevIntegration extends Integration {

  override def getName() : String = {"chebyshev"}

  override def approximate(a:Double, b:Double, n:Int) : Double = {
    var result:Double = 0.0

    val h:Double = (abs(b) + abs(a))/n
    for(i <- 1 to n){
      result += f(a + (i-1)*h) * h + f(a + i*h) * h
    }

    return result/2
  }
}
