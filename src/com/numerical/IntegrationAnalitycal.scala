package com.numerical

class IntegrationAnalitycal extends Integration{

  override def getName() : String = {"analytical"}

  override def approximate(a:Double, b:Double, n:Int) : Double = {
    return F(b) - F(a)
  }
}
