package com.numerical

import math._

trait Integration {
  def F(x:Double) : Double = {
    return (12.0/42.0)*pow(x,7.0) - pow(x,6.0) - 2*pow(x,5.0) + 7.5*pow(x,4.0) + (112.0/42.0)*pow(x,3) - 15*pow(x,2.0) + 40*x
  }

  //derivative of F(x)
  def f(x:Double) : Double = {
    return 2.0*pow(x,6.0) - 6.0*pow(x,5.0) - 10.0*pow(x,4.0) + 30.0*pow(x,3.0) + 8.0*pow(x,2.0) - 30.0*x + 40.0
  }

  def getName() : String

  def approximate(a:Double, b:Double, n:Int) : Double
}