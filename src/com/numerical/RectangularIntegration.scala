package com.numerical

import scala.math._

class RectangularIntegration extends Integration {

  override def getName() : String = {"rectangular"}

  override def approximate(a:Double, b:Double, n:Int) : Double = {
    var result:Double = 0.0

    val h:Double = (abs(b) + abs(a))/n
    for(i <- 0 to n-1){
      result += f(a + i*h) * h
    }

    return result
  }
}
